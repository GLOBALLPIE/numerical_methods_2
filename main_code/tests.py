from main_code.integration import quad, moments, doubling_nc, optimal_nc
import numpy as np
# import scipy.integrate as integrate
from scipy.linalg import hankel
from scipy.special import jacobi
from main_code.test_integration import test_nc_convergence

xl = 1
xr = 2
n = 5
params = [xl, xr, 4 / 7, 0]
nodes = np.linspace(xl, xr, n)

mu = moments(2 * len(nodes) - 1, xl, xr, *params)  # Правильно
# vandermond = np.rot90(np.vander(nodes))
# coefficients = np.linalg.solve(vandermond, mu)
#
f = lambda x: np.cos(x)

# f_values = [f(node) for node in nodes]
#
# print(mu.shape)
# print(nodes)
# print(np.dot(coefficients, f_values))
# # print(coefficients)
# params = np.array(params)
# print(params[params % 2 != 0])
# print(mu, '\n')
# print(mu[:n])
# print(mu[n:])

# hankel_matrix = hankel(mu[:n], mu[n:])
# print(hankel_matrix)
# b = -np.array(mu[n:])
# coefficents = np.linalg.solve(hankel_matrix, b)
# coefficents = np.flip(coefficents)
# coefficents = np.concatenate((np.array([1]), coefficents))

# print('mu: ', mu, '\n')
# hankel_matrix = hankel(mu[:n], mu[n-1:-1])
# print('hankel_matrix: ', hankel_matrix, '\n')
# b = -np.array(mu[n:])
# print('b: ', b, '\n')
# polinom_coefficents = np.linalg.solve(hankel_matrix, b)
# print('polinom_coefficents: ', polinom_coefficents, '\n')
# polinom_coefficents = np.flip(polinom_coefficents)
# polinom_coefficents = np.concatenate((np.array([1]), polinom_coefficents))
# print('polinom_coefficents: ', polinom_coefficents, '\n')
# roots = np.roots(polinom_coefficents)
# print('roots: ', roots, '\n')
# vandermond = np.rot90(np.vander(roots))
# print('vandermond: ', vandermond, '\n')
# coefficents = np.linalg.solve(vandermond, mu[:n])
# print('coefficents: ', coefficents, '\n')
# f_values = [f(root) for root in roots]
# print('f_values: ', f_values, '\n')
# print(np.dot(coefficents,f_values))
# print(hankel([1, 2, 3, 4, 5], [5, 6, 7, 8, 9]),'\n')
# print(mu, '\n')
# print(hankel(mu[:n], mu[n - 1:-1]))

# print(np.log(np.e ** 2))
# a = np.array([[2, 1], [3, 4]])
# b = np.array([[2, 1]])
# print(a @ b)

# L = 1
# if not (L > 1):
#     raise ValueError('L should be > 1')

# n = 5
# print(n - 1 if n % 2 == 0 else n)

# print(doubling_nc(f, xl, xr, n, 1e-6, *params))

# print(optimal_nc(f, xl, xr, n, 1e-8, *params))


# print(moments(n, xl, xr, *params))
alpha = 4 / 7
a = xl
f = lambda x: x ** 3 / ((x - xl) ** alpha)

from scipy.special import comb


def mom(max_s: int, xl: float, xr: float, a: float = 0.0, b: float = 1.0, alpha: float = 0.0, beta: float = 0.0):
    mu = np.zeros(max_s + 1)
    if alpha != 0:
        mu[0] = ((xr - a) ** (1 - alpha) - (xl - a) ** (1 - alpha)) / (1 - alpha)
        addendum = 0
        for i in range(1, max_s + 1):
            for j in range(i):
                addendum += (-1) ** j * comb(i, j + 1) * (a ** (j + 1)) * mu[i - (j + 1)]
            mu[i] = ((xr - a) ** ((i + 1) - alpha) - (xl - a) ** ((i + 1) - alpha)) / ((i + 1) - alpha)
            mu[i] += addendum
            addendum = 0
        return mu
    if beta != 0:
        temp = -xr
        xr = -xl
        xl = temp
        mu[0] = ((xr + b) ** (1 - beta) - (xl + b) ** (1 - beta)) / (1 - beta)
        addendum = 0
        for i in range(1, max_s + 1):
            for j in range(i):
                addendum += (-1) ** (2 * j + 1) * comb(i, j + 1) * (b ** (j + 1)) * mu[i - (j + 1)]
            mu[i] = ((xr + b) ** ((i + 1) - beta) - (xl + b) ** ((i + 1) - beta)) / ((i + 1) - beta)
            mu[i] += addendum
            # mu[i] *= (-1) ** i
            addendum = 0
        for i in range(max_s + 1):
            mu[i] *= (-1) ** i
        return mu


m1 = moments(10, xl, xr, a=1, alpha=2 / 7)
m2 = mom(10, xl, xr, a=1, alpha=2 / 7)
print(m1)
print(m2)
print(m1 - m2)
