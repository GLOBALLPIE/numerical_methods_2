import numpy as np
from scipy.linalg import hankel
from scipy.special import comb
import math

MAXITER = 12


def compute_special_binomial(index, xl, xr, coefs):
    if index == 0:
        return 1
    sum = 0
    sum += np.math.factorial(index) * xl ** index
    div_factorial = 1
    for i in range(1, index + 1):
        div_factorial *= i
        sum += ((-1) ** i) * (np.math.factorial(index) / div_factorial) * coefs[i - 1] * (xr ** i) * (
                xl ** (index - i))
    return sum


def moments(max_s: int, xl: float, xr: float, a: float = 0.0, b: float = 1.0, alpha: float = 0.0, beta: float = 0.0):
    # Done
    """
    compute moments of the weight 1 / (x-a)^alpha / (b-x)^beta over [xl, xr]
    max_s : highest required order
    xl : left limit
    xr : right limit
    a : weight parameter a
    b : weight parameter b
    alpha : weight parameter alpha
    beta : weight parameter beta
    """

    assert alpha * beta == 0, \
        f'alpha ({alpha}) and/or beta ({beta}) must be 0'

    if alpha == 0 and beta == 0:
        return [(xr ** s - xl ** s) / s for s in range(1, max_s + 2)]

    mu = np.zeros(max_s + 1)

    if alpha != 0:
        mu[0] = ((xr - a) ** (1 - alpha) - (xl - a) ** (1 - alpha)) / (1 - alpha)
        addendum = 0
        for i in range(1, max_s + 1):
            for j in range(i):
                addendum += (-1) ** j * comb(i, j + 1) * (a ** (j + 1)) * mu[i - (j + 1)]
            mu[i] = ((xr - a) ** ((i + 1) - alpha) - (xl - a) ** ((i + 1) - alpha)) / ((i + 1) - alpha)
            mu[i] += addendum
            addendum = 0
        return mu
    if beta != 0:
        temp = -xr
        xr = -xl
        xl = temp
        mu[0] = ((xr + b) ** (1 - beta) - (xl + b) ** (1 - beta)) / (1 - beta)
        addendum = 0
        for i in range(1, max_s + 1):
            for j in range(i):
                addendum += (-1) ** (2 * j + 1) * comb(i, j + 1) * (b ** (j + 1)) * mu[i - (j + 1)]
            mu[i] = ((xr + b) ** ((i + 1) - beta) - (xl + b) ** ((i + 1) - beta)) / ((i + 1) - beta)
            mu[i] += addendum
            # mu[i] *= (-1) ** i
            addendum = 0
        for i in range(max_s + 1):
            mu[i] *= (-1) ** i
        return mu

    raise NotImplementedError
    return mu


def quad(f, xl: float, xr: float, nodes, *params):
    """
    small Newton—Cotes formula
    f: function to integrate
    xl : left limit
    xr : right limit
    nodes: nodes within [xl, xr]
    *params: parameters of the variant — a, b, alpha, beta)
    """
    mu = moments(len(nodes) - 1, xl, xr, *params)
    vandermond = np.rot90(np.vander(nodes))
    coefficients = np.linalg.solve(vandermond, mu)

    f_values = np.array([f(node) for node in nodes])
    return np.dot(coefficients, f_values)

    raise NotImplementedError
    # return # small formula result over [xl, xr]


def quad_gauss(f, xl: float, xr: float, n: int, *params):
    """
    small Gauss formula
    f: function to integrate
    xl : left limit
    xr : right limit
    n : number of nodes
    *params: parameters of the variant — a, b, alpha, beta)
    """
    mu = np.array(moments(2 * n - 1, xl, xr, *params))  # 2*n-1, чтобы получить 2*n моментов
    hankel_matrix = hankel(mu[:n], mu[n - 1:-1])
    # assert hankel_matrix.shape[0] == hankel_matrix.shape[1], 'Hankel matrix must be square'
    b = -np.array(mu[n:])
    polinom_coefficents = np.linalg.solve(hankel_matrix, b)

    polinom_coefficents = np.flip(polinom_coefficents)
    polinom_coefficents = np.concatenate((np.array([1]), polinom_coefficents))

    roots = np.roots(polinom_coefficents)
    vandermond = np.rot90(np.vander(roots))
    coefficents = np.linalg.solve(vandermond, mu[:n])
    f_values = [f(root) for root in roots]

    return np.dot(coefficents, f_values)

    raise NotImplementedError
    # return # small formula result over [xl, xr]


def composite_quad(f, xl: float, xr: float, N: int, n: int, *params):
    """
    composite Newton—Cotes formula
    f: function to integrate
    xl : left limit
    xr : right limit
    N : number of steps
    n : number of nodes od small formulae
    *params: parameters of the variant — a, b, alpha, beta)
    """
    mesh = np.linspace(xl, xr, N + 1)
    return sum(quad(f, mesh[i], mesh[i + 1], equidist(n, mesh[i], mesh[i + 1]), *params) for i in range(N))


def composite_gauss(f, a: float, b: float, N: int, n: int, *params):
    """
    composite Gauss formula
    f: function to integrate
    xl : left limit
    xr : right limit
    N : number of steps
    n : number of nodes od small formulae
    *params: parameters of the variant — a, b, alpha, beta)
    """
    mesh = np.linspace(a, b, N + 1)
    return sum(quad_gauss(f, mesh[i], mesh[i + 1], n, *params) for i in range(N))


def equidist(n: int, xl: float, xr: float):
    if n == 1:
        return [0.5 * (xl + xr)]
    else:
        return np.linspace(xl, xr, n)


def runge(s1: float, s2: float, L: float, m: float):
    """ estimate m-degree error for s2 """
    if not (L > 1):
        raise ValueError('L should be > 1')
    return (s2 - s1) / (L ** m - 1)
    raise NotImplementedError


def aitken(s1: float, s2: float, s3: float, L: float):
    """
    estimate convergence order
    s1, s2, s3: consecutive composite quads
    return: convergence order estimation
    """
    # if NON MONOTONOUS CONVERGENCE
    #    return -1
    if not (L > 1):
        raise ValueError('L should be > 1')

    value = (s3 - s2) / (s2 - s1)
    if value < 0:
        return -1

    m = -np.log(value) / np.log(L)
    return m

    raise NotImplementedError


def doubling_nc(f, xl: float, xr: float, n: int, tol: float, *params):
    """
    compute integral by doubling the steps number with theoretical convergence rate
    f : function to integrate
    xl : left limit
    xr : right limit
    n : nodes number in the small formula
    tol : error tolerance
    *params : arguments to pass to composite_quad function
    """
    # required local variables to return
    # S : computed value of the integral with required tolerance
    # N : number of steps for S     (кол-во делений отрезка)
    # err : estimated error of S
    # iter : number of iterations (steps doubling)
    iter = 0
    err = 1
    N = 1  # Удваивая число шагов, уменьшаем в два раза длину шага
    while 1:
        if iter == MAXITER:
            print("Convergence not reached!")
            return 0, 0, 10 * tol
        iter += 1
        s1 = composite_quad(f, xl, xr, N, n, *params)
        # TODO: Можно сохранять значение, чтобы не пересчитывать
        s2 = composite_quad(f, xl, xr, 2 * N, n, *params)
        teor_m = n - 1 if n % 2 == 0 else n
        err = abs(runge(s1, s2, 2, teor_m))

        if err < tol:
            # TODO: ValueError: too many values to unpack (появляется в тесте, если возвращать больше значений)
            return 2 * N, s2, err

        N *= 2

    raise NotImplementedError
    return N, S, err


def doubling_nc_aitken(f, xl: float, xr: float, n: int, tol: float, *params):
    """
    compute integral by doubling the steps number with Aitken estimation of the convergence rate
    f : function to integrate
    xl : left limit
    xr : right limit
    n : nodes number in the small formula
    tol : error tolerance
    *params : arguments to pass to composite_quad function
    """
    # required local variables to return
    # S : computed value of the integral with required tolerance
    # N : number of steps for S
    # err : estimated error of S
    # m : estimated convergence rate by Aitken for S
    # iter : number of iterations (steps doubling)
    iter = 0
    err = 1
    N = 1  # Удваивая число шагов, уменьшаем в два раза длину шага
    while 1:
        if iter == MAXITER:
            print("Convergence not reached!")
            return 0, 0, 10 * tol, -100
        iter += 1
        s1 = composite_quad(f, xl, xr, N, n, *params)
        # TODO: Можно сохранять значение, чтобы не пересчитывать
        s2 = composite_quad(f, xl, xr, 2 * N, n, *params)
        s3 = composite_quad(f, xl, xr, 4 * N, n, *params)
        m = aitken(s1, s2, s3, L=2)
        teor_m = (n - 1 if n % 2 == 0 else n) + 1

        if m <= 0 or abs(m - teor_m) > 2:
            m = teor_m

        err = abs(runge(s1, s2, 2, m))

        if err < tol:
            return 2 * N, s2, err, m

        N *= 2

    raise NotImplementedError
    return N, S, err, m


def doubling_gauss(f, xl: float, xr: float, n: int, tol: float, *params):
    """
    compute integral by doubling the steps number with theoretical convergence rate
    f : function to integrate
    xl : left limit
    xr : right limit
    n : nodes number in the small formula
    tol : error tolerance
    *params : arguments to pass to composite_quad function
    """
    # required local variables to return
    # S : computed value of the integral with required tolerance
    # N : number of steps for S
    # err : estimated error of S
    # iter : number of iterations (steps doubling)
    iter = 0
    err = 1
    N = 1  # Удваивая число шагов, уменьшаем в два раза длину шага
    while 1:
        if iter == MAXITER:
            print("Convergence not reached!")
            return 0, 0, 10 * tol
        iter += 1
        s1 = composite_gauss(f, xl, xr, N, n, *params)
        # TODO: Можно сохранять значение, чтобы не пересчитывать
        s2 = composite_gauss(f, xl, xr, 2 * N, n, *params)
        teor_m = (2 * n - 1) + 1
        err = abs(runge(s1, s2, 2, teor_m))

        if err < tol:
            return 2 * N, s2, err

        N *= 2

    raise NotImplementedError
    return N, S, err


def doubling_gauss_aitken(f, xl: float, xr: float, n: int, tol: float, *params):
    """
    compute integral by doubling the steps number with Aitken estimation of the convergence rate
    f : function to integrate
    xl : left limit
    xr : right limit
    n : nodes number in the small formula
    tol : error tolerance
    *params : arguments to pass to composite_quad function
    """
    # required local variables to return
    # S : computed value of the integral with required tolerance
    # N : number of steps for S
    # err : estimated error of S
    # m : estimated convergence rate by Aitken for S
    # iter : number of iterations (steps doubling)
    iter = 0
    err = 1
    N = 1  # Удваивая число шагов, уменьшаем в два раза длину шага
    while 1:
        if iter == MAXITER:
            print("Convergence not reached!")
            return 0, 0, 10 * tol, -100
        iter += 1
        s1 = composite_gauss(f, xl, xr, N, n, *params)
        # TODO: Можно сохранять значение, чтобы не пересчитывать
        s2 = composite_gauss(f, xl, xr, 2 * N, n, *params)
        s3 = composite_gauss(f, xl, xr, 4 * N, n, *params)
        m = aitken(s1, s2, s3, L=2)
        teor_m = (2 * n - 1) + 1

        if m <= 0 or abs(m - teor_m) > 2:
            m = teor_m

        err = abs(runge(s1, s2, 2, m))

        if err < tol:
            return 2 * N, s2, err, m

        N *= 2

    raise NotImplementedError
    return N, S, err, m


def optimal_nc(f, xl: float, xr: float, n: int, tol: float, *params):
    """ estimate the optimal step with Aitken and Runge procedures
    f : function to integrate
    xl : left limit
    xr : right limit
    n : nodes number in the small formula
    tol : error tolerance
    *params : arguments to pass to composite_quad function
    """
    # required local variables to return
    # S : computed value of the integral with required tolerance
    # N : number of steps for S
    # err : estimated error of S
    # iter : number of iterations (steps doubling)
    fac = 0.95
    iter = 0
    N = 1
    L = 2
    err = 1
    h_opt = xr - xl
    while 1:
        if iter == MAXITER:
            print("Convergence not reached!")
            return 0, 0, 10 * tol
        iter += 1
        s1 = composite_quad(f, xl, xr, N, n, *params)
        s2 = composite_quad(f, xl, xr, 2 * N, n, *params)
        s3 = composite_quad(f, xl, xr, 4 * N, n, *params)
        m = aitken(s1, s2, s3, L)
        teor_m = (n - 1 if n % 2 == 0 else n) + 1

        if m <= 0 or abs(m - teor_m) > 2:
            m = teor_m

        err = abs(runge(s1, s2, L, m))
        h_opt = (xr - xl) / (2 * N) * pow(tol / abs(err), 1 / m)  # Можно использовать np.power()
        h_opt *= fac
        N = math.ceil((xr - xl) / h_opt)
        h_opt = (xr - xl) / N
        S_opt1 = composite_quad(f, xl, xr, N, n, *params)
        S_opt2 = composite_quad(f, xl, xr, 2 * N, n, *params)
        err_opt = abs((S_opt2 - S_opt1) / (1 - L ** (-m)))
        if err < tol and err_opt < tol:
            return N, S_opt1, err_opt
        N *= 2
    raise NotImplementedError
    return N, S, err
